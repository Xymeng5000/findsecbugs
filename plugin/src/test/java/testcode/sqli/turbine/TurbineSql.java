package testcode.sqli.turbine;

import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.om.security.peer.GroupPeer;

public class TurbineSql {

    public void injection1(BasePeer peer, String injection) {
        BasePeer.executeQuery(injection);
        BasePeer.executeQuery(injection,false,null);
        BasePeer.executeQuery(injection,0,0,false,null);
        BasePeer.executeQuery(injection,0,0,"",false);
        BasePeer.executeQuery(injection,"");
        BasePeer.executeQuery(injection,"",false);
    }

    public void injection2(GroupPeer peer, String injection) {
        BasePeer.executeQuery(injection);
        BasePeer.executeQuery(injection,false,null);
        BasePeer.executeQuery(injection,0,0,false,null);
        BasePeer.executeQuery(injection,0,0,"",false);
        BasePeer.executeQuery(injection,"");
        BasePeer.executeQuery(injection,"",false);
    }

    public void injection3(String injection) {
        BasePeer.executeQuery(injection);
        BasePeer.executeQuery(injection,false,null);
        BasePeer.executeQuery(injection,0,0,false,null);
        BasePeer.executeQuery(injection,0,0,"",false);
        BasePeer.executeQuery(injection,"");
        BasePeer.executeQuery(injection,"",false);
    }

    public void injection4(String injection) {
        GroupPeer.executeQuery(injection);
        GroupPeer.executeQuery(injection,false,null);
        GroupPeer.executeQuery(injection,0,0,false,null);
        GroupPeer.executeQuery(injection,0,0,"",false);
        GroupPeer.executeQuery(injection,"");
        GroupPeer.executeQuery(injection,"",false);
    }

    public void falsePositive(BasePeer peer) {
        String constantValue = "SELECT * FROM test";

        BasePeer.executeQuery(constantValue);
        BasePeer.executeQuery(constantValue,false,null);
        BasePeer.executeQuery(constantValue,0,0,false,null);
        BasePeer.executeQuery(constantValue,0,0,"",false);
        BasePeer.executeQuery(constantValue,"");
        BasePeer.executeQuery(constantValue,"",false);
    }
}
