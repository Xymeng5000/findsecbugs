package testcode.sqli.turbine;

import org.apache.torque.util.BasePeer;

public class TorqueSql {

    public void injection1(BasePeer peer, String injection) {
        BasePeer.executeQuery(injection);
        BasePeer.executeQuery(injection,false,null);
        BasePeer.executeQuery(injection,0,0,false,null);
        BasePeer.executeQuery(injection,0,0,"",false);
        BasePeer.executeQuery(injection,"");
        BasePeer.executeQuery(injection,"",false);
    }

    public void injection2(String injection) {
        BasePeer.executeQuery(injection);
        BasePeer.executeQuery(injection,false,null);
        BasePeer.executeQuery(injection,0,0,false,null);
        BasePeer.executeQuery(injection,0,0,"",false);
        BasePeer.executeQuery(injection,"");
        BasePeer.executeQuery(injection,"",false);
    }

    public void falsePositive(BasePeer peer) {
        String constantValue = "SELECT * FROM test";

        BasePeer.executeQuery(constantValue);
        BasePeer.executeQuery(constantValue,false,null);
        BasePeer.executeQuery(constantValue,0,0,false,null);
        BasePeer.executeQuery(constantValue,0,0,"",false);
        BasePeer.executeQuery(constantValue,"");
        BasePeer.executeQuery(constantValue,"",false);
    }
}
