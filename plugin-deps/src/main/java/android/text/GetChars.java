package android.text;

public interface GetChars
        extends CharSequence
{
    /**
     * Exactly like String.getChars(): copy chars <code>start</code>
     * through <code>end - 1</code> from this CharSequence into <code>dest</code>
     * beginning at offset <code>destoff</code>.
     */
    public void getChars(int start, int end, char[] dest, int destoff);
}