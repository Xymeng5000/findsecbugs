package android.text;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.style.CharacterStyle;
import android.util.Log;




import static android.content.ContentValues.TAG;

public class TextUtils {



    public static final int ALIGNMENT_SPAN = 1;
    /** @hide */
    public static final int FIRST_SPAN = ALIGNMENT_SPAN;
    /** @hide */
    public static final int FOREGROUND_COLOR_SPAN = 2;
    /** @hide */
    public static final int RELATIVE_SIZE_SPAN = 3;
    /** @hide */
    public static final int SCALE_X_SPAN = 4;
    /** @hide */
    public static final int STRIKETHROUGH_SPAN = 5;
    /** @hide */
    public static final int UNDERLINE_SPAN = 6;
    /** @hide */
    public static final int STYLE_SPAN = 7;
    /** @hide */
    public static final int BULLET_SPAN = 8;
    /** @hide */
    public static final int QUOTE_SPAN = 9;
    /** @hide */
    public static final int LEADING_MARGIN_SPAN = 10;
    /** @hide */
    public static final int URL_SPAN = 11;
    /** @hide */
    public static final int BACKGROUND_COLOR_SPAN = 12;
    /** @hide */
    public static final int TYPEFACE_SPAN = 13;
    /** @hide */
    public static final int SUPERSCRIPT_SPAN = 14;
    /** @hide */
    public static final int SUBSCRIPT_SPAN = 15;
    /** @hide */
    public static final int ABSOLUTE_SIZE_SPAN = 16;
    /** @hide */
    public static final int TEXT_APPEARANCE_SPAN = 17;
    /** @hide */
    public static final int ANNOTATION = 18;
    /** @hide */
    public static final int SUGGESTION_SPAN = 19;
    /** @hide */
    public static final int SPELL_CHECK_SPAN = 20;
    /** @hide */
    public static final int SUGGESTION_RANGE_SPAN = 21;
    /** @hide */
    public static final int EASY_EDIT_SPAN = 22;
    /** @hide */
    public static final int LOCALE_SPAN = 23;
    /** @hide */
    public static final int TTS_SPAN = 24;
    /** @hide */
    public static final int LAST_SPAN = TTS_SPAN;


    public static boolean isEmpty(CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }
    private static void writeWhere(Parcel p, Spanned sp, Object o) {
        p.writeInt(sp.getSpanStart(o));
        p.writeInt(sp.getSpanEnd(o));
        p.writeInt(sp.getSpanFlags(o));
    }
    public static void writeToParcel(CharSequence cs, Parcel p, int parcelableFlags) {
        if (cs instanceof Spanned) {
            p.writeInt(0);
            p.writeString(cs.toString());

            Spanned sp = (Spanned) cs;
            Object[] os = sp.getSpans(0, cs.length(), Object.class);

            // note to people adding to this: check more specific types
            // before more generic types.  also notice that it uses
            // "if" instead of "else if" where there are interfaces
            // so one object can be several.

            for (int i = 0; i < os.length; i++) {
                Object o = os[i];
                Object prop = os[i];

                if (prop instanceof CharacterStyle) {
                    prop = ((CharacterStyle) prop).getUnderlying();
                }

                if (prop instanceof ParcelableSpan) {
                    final ParcelableSpan ps = (ParcelableSpan) prop;
                    final int spanTypeId = ps.getSpanTypeIdInternal();
                    if (spanTypeId < FIRST_SPAN || spanTypeId > LAST_SPAN) {
                        Log.e(TAG, "External class \"" + ps.getClass().getSimpleName()
                                + "\" is attempting to use the frameworks-only ParcelableSpan"
                                + " interface");
                    } else {
                        p.writeInt(spanTypeId);
                        ps.writeToParcelInternal(p, parcelableFlags);
                        writeWhere(p, sp, o);
                    }
                }
            }

            p.writeInt(0);
        } else {
            p.writeInt(1);
            if (cs != null) {
                p.writeString(cs.toString());
            } else {
                p.writeString(null);
            }
        }
    }

    private static void readSpan(Parcel p, Spannable sp, Object o) {
        sp.setSpan(o, p.readInt(), p.readInt(), p.readInt());
    }



    public static final Parcelable.Creator<CharSequence> CHAR_SEQUENCE_CREATOR
            = new Parcelable.Creator<CharSequence>() {
        /**
         * Read and return a new CharSequence, possibly with styles,
         * from the parcel.
         */
        public CharSequence createFromParcel(Parcel p) {
            int kind = p.readInt();

            String string = p.readString();
            if (string == null) {
                return null;
            }

            if (kind == 1) {
                return string;
            }

            SpannableString sp = new SpannableString(string);

            while (true) {
                kind = p.readInt();

                if (kind == 0)
                    break;

                switch (kind) {
                    case ALIGNMENT_SPAN:
                       // readSpan(p, sp, new AlignmentSpan.Standard(p));
                        break;

                    case FOREGROUND_COLOR_SPAN:
                       // readSpan(p, sp, new ForegroundColorSpan(p));
                        break;

                    case RELATIVE_SIZE_SPAN:
                       // readSpan(p, sp, new RelativeSizeSpan(p));
                        break;

                    case SCALE_X_SPAN:
                      //  readSpan(p, sp, new ScaleXSpan(p));
                        break;

                    case STRIKETHROUGH_SPAN:
                       // readSpan(p, sp, new StrikethroughSpan(p));
                        break;

                    case UNDERLINE_SPAN:
                       // readSpan(p, sp, new UnderlineSpan(p));
                        break;

                    case STYLE_SPAN:
                       // readSpan(p, sp, new StyleSpan(p));
                        break;

                    case BULLET_SPAN:
                       // readSpan(p, sp, new BulletSpan(p));
                        break;

                    case QUOTE_SPAN:
                       // readSpan(p, sp, new QuoteSpan(p));
                        break;

                    case LEADING_MARGIN_SPAN:
                       // readSpan(p, sp, new LeadingMarginSpan.Standard(p));
                        break;

                    case URL_SPAN:
                       // readSpan(p, sp, new URLSpan(p));
                        break;

                    case BACKGROUND_COLOR_SPAN:
                        //readSpan(p, sp, new BackgroundColorSpan(p));
                        break;

                    case TYPEFACE_SPAN:
                       // readSpan(p, sp, new TypefaceSpan(p));
                        break;

                    case SUPERSCRIPT_SPAN:
                       // readSpan(p, sp, new SuperscriptSpan(p));
                        break;

                    case SUBSCRIPT_SPAN:
                        //readSpan(p, sp, new SubscriptSpan(p));
                        break;

                    case ABSOLUTE_SIZE_SPAN:
                        //readSpan(p, sp, new AbsoluteSizeSpan(p));
                        break;

                    case TEXT_APPEARANCE_SPAN:
                        //readSpan(p, sp, new TextAppearanceSpan(p));
                        break;

                    case ANNOTATION:
                        //readSpan(p, sp, new Annotation(p));
                        break;

                    case SUGGESTION_SPAN:
                      //  readSpan(p, sp, new SuggestionSpan(p));
                        break;

                    case SPELL_CHECK_SPAN:
                      //  readSpan(p, sp, new SpellCheckSpan(p));
                        break;

                    case SUGGESTION_RANGE_SPAN:
                        //readSpan(p, sp, new SuggestionRangeSpan(p));
                        break;

                    case EASY_EDIT_SPAN:
                       // readSpan(p, sp, new EasyEditSpan(p));
                        break;

                    case LOCALE_SPAN:
                        //readSpan(p, sp, new LocaleSpan(p));
                        break;

                    case TTS_SPAN:
                      //  readSpan(p, sp, new TtsSpan(p));
                        break;

                    default:
                        throw new RuntimeException("bogus span encoding " + kind);
                }
            }

            return sp;
        }

        public CharSequence[] newArray(int size)
        {
            return new CharSequence[size];
        }
    };

}
