package android.os;

public interface Parcelable
{
    int CONTENTS_FILE_DESCRIPTOR = 1;
    int PARCELABLE_WRITE_RETURN_VALUE = 1;

    int describeContents();

    void writeToParcel(Parcel paramParcel, int paramInt);

    interface ClassLoaderCreator<T>
            extends Parcelable.Creator<T>
    {
        T createFromParcel(Parcel paramParcel, ClassLoader paramClassLoader);
    }

    interface Creator<T>
    {
        T createFromParcel(Parcel paramParcel);

        T[] newArray(int paramInt);
    }
}