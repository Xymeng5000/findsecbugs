package android.support.v7.app;



public class ActionBarDrawerToggle {
    public interface DelegateProvider {

        Delegate getDrawerToggleDelegate();
    }

    public interface Delegate {


       // void setActionBarUpIndicator(Drawable upDrawable,  int contentDescRes);


        void setActionBarDescription( int contentDescRes);

        /**
         * Returns the drawable to be set as up button when DrawerToggle is disabled
         */
        //Drawable getThemeUpIndicator();

        /**
         * Returns the context of ActionBar
         */
        //Context getActionBarThemedContext();

        /**
         * Returns whether navigation icon is visible or not.
         * Used to print warning messages in case developer forgets to set displayHomeAsUp to true
         */
        boolean isNavigationVisible();
    }
}
