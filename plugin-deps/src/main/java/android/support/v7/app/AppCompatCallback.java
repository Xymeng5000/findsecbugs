package android.support.v7.app;


import android.support.v7.view.ActionMode;

public interface AppCompatCallback {

    /**
     * Called when a support action mode has been started.
     *
     * @param mode The new action mode.
     */
    void onSupportActionModeStarted(ActionMode mode);

    /**
     * Called when a support action mode has finished.
     *
     * @param mode The action mode that just finished.
     */
    void onSupportActionModeFinished(ActionMode mode);

    /**
     * Called when a support action mode is being started for this window. Gives the
     * callback an opportunity to handle the action mode in its own unique and
     * beautiful way. If this method returns null the system can choose a way
     * to present the mode or choose not to start the mode at all.
     *
     * @param callback Callback to control the lifecycle of this action mode
     * @return The ActionMode that was started, or null if the system should present it
     */

    ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback);

}
