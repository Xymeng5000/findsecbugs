

package android.support.v7.app;



import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.view.ActionMode;



public class AppCompatActivity extends FragmentActivity implements AppCompatCallback,
        TaskStackBuilder.SupportParentable, ActionBarDrawerToggle.DelegateProvider {

    private AppCompatDelegate mDelegate;


    protected void onCreate( Bundle savedInstanceState) {

    }

    @Override
    public void onSupportActionModeStarted( ActionMode mode) {
    }

    @Override
    public void onSupportActionModeFinished(ActionMode mode) {

    }

    @Override
    public ActionMode onWindowStartingSupportActionMode( ActionMode.Callback callback) {
        return null;
    }


    @Override
    public Intent getSupportParentActivityIntent() {
        return null;
    }

    @Override
    public ActionBarDrawerToggle.Delegate getDrawerToggleDelegate() {
        return null;
    }


    public void setContentView(int layoutResID) {

    }

    public AppCompatDelegate getDelegate() {

        return mDelegate;
    }
}
