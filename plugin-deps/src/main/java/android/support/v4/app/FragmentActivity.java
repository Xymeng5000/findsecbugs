/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.support.v4.app;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.content.res.Configuration;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
;
import android.os.Parcelable;

import android.util.Log;

import android.view.View;


import java.io.FileDescriptor;
import java.io.PrintWriter;


public class FragmentActivity {
    private static final String TAG = "FragmentActivity";

    static final String FRAGMENTS_TAG = "android:support:fragments";
    static final String NEXT_CANDIDATE_REQUEST_INDEX_TAG = "android:support:next_request_index";
    static final String ALLOCATED_REQUEST_INDICIES_TAG = "android:support:request_indicies";
    static final String REQUEST_FRAGMENT_WHO_TAG = "android:support:request_fragment_who";
    static final int MAX_NUM_PENDING_FRAGMENT_ACTIVITY_RESULTS = 0xffff - 1;

    static final int MSG_REALLY_STOPPED = 1;
    static final int MSG_RESUME_PENDING = 2;


    boolean mCreated;
    boolean mResumed;
    boolean mStopped;
    boolean mReallyStopped;
    boolean mRetaining;

    boolean mOptionsMenuInvalidated;
    boolean mRequestedPermissionsFromFragment;


    static final class NonConfigurationInstances {

    }

    // ------------------------------------------------------------------------
    // HOOKS INTO ACTIVITY
    // ------------------------------------------------------------------------

    /**
     * Dispatch incoming result to the correct fragment.
     */

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */

    public void onBackPressed() {

    }


    public void startActivityForResult(Intent intent, int requestCode) {

    }


    public final void validateRequestPermissionsRequestCode(int requestCode) {

    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

    }

}



