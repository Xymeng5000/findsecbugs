package android.support.v4.app;

import android.content.Intent;
public class TaskStackBuilder {

    public interface SupportParentable {
        Intent getSupportParentActivityIntent();
    }
}
