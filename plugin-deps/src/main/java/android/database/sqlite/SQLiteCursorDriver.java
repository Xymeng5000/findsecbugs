package android.database.sqlite;

import android.database.Cursor;

public interface SQLiteCursorDriver
{
    Cursor query(SQLiteDatabase.CursorFactory paramCursorFactory, String[] paramArrayOfString);

    void cursorDeactivated();

    void cursorRequeried(Cursor paramCursor);

    void cursorClosed();

    void setBindArguments(String[] paramArrayOfString);
}
