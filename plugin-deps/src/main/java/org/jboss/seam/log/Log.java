package org.jboss.seam.log;

/**
 * https://github.com/seam2/jboss-seam/blob/f3077fee9d04b2b3545628cd9e6b58c859feb988/jboss-seam/src/main/java/org/jboss/seam/log/Log.java
 */
public interface Log {
    void trace(Object object, Object... params);
    void trace(Object object, Throwable t, Object... params);
    void debug(Object object, Object... params);
    void debug(Object object, Throwable t, Object... params);
    void info(Object object, Object... params);
    void info(Object object, Throwable t, Object... params);
    void warn(Object object, Object... params);
    void warn(Object object, Throwable t, Object... params);
    void error(Object object, Object... params);
    void error(Object object, Throwable t, Object... params);
    void fatal(Object object, Object... params);
    void fatal(Object object, Throwable t, Object... params);
}
