package org.springframework.expression;

public interface Expression {

    <T> T getValue(Class<T> stringClass);

    <T> T getValue(EvaluationContext context, Class<T> stringClass);
}
