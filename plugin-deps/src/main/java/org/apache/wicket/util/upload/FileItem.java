package org.apache.wicket.util.upload;

public interface FileItem {

    String getName();
}
