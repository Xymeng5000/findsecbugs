package org.apache.commons.fileupload;

public interface FileItem {

    String getName();
}
